﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAncors : MonoBehaviour
{
    //------------Top-------------------
    public void topLeft(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(0, 1);
        uitransform.anchorMax = new Vector2(0, 1);
        uitransform.pivot = new Vector2(0, 1);
    }

    public void topMiddle(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(0.5f, 1);
        uitransform.anchorMax = new Vector2(0.5f, 1);
        uitransform.pivot = new Vector2(0.5f, 1);
    }


    public void topRight(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(1, 1);
        uitransform.anchorMax = new Vector2(1, 1);
        uitransform.pivot = new Vector2(1, 1);
    }

    //------------Middle-------------------
    public void middleLeft(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(0, 0.5f);
        uitransform.anchorMax = new Vector2(0, 0.5f);
        uitransform.pivot = new Vector2(0, 0.5f);
    }

    public void middle(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(0.5f, 0.5f);
        uitransform.anchorMax = new Vector2(0.5f, 0.5f);
        uitransform.pivot = new Vector2(0.5f, 0.5f);
    }

    public void middleRight(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(1, 0.5f);
        uitransform.anchorMax = new Vector2(1, 0.5f);
        uitransform.pivot = new Vector2(1, 0.5f);
    }

    //------------Bottom-------------------
    public void bottomLeft(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(0, 0);
        uitransform.anchorMax = new Vector2(0, 0);
        uitransform.pivot = new Vector2(0, 0);
    }

    public void bottomMiddle(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(0.5f, 0);
        uitransform.anchorMax = new Vector2(0.5f, 0);
        uitransform.pivot = new Vector2(0.5f, 0);
    }

    public void bottomRight(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(1, 0);
        uitransform.anchorMax = new Vector2(1, 0);
        uitransform.pivot = new Vector2(1, 0);
    }
}
