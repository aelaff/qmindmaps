﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MapController : MonoBehaviour
{
    public Image map;
    public GameObject loading;
    public ArabicText suraTitleTxt;
    public Transform follower,rootFollower,ayatFollower;
    public static MapController instance;
    void Start()
    {
        instance = this;
        suraTitleTxt.Text ="سورة : "+ PlayerPrefs.GetString(Constants.SuraTitle);
        //loading.SetActive(true);
        StartCoroutine(DownloadImage(PlayerPrefs.GetString(Constants.MapLink)));
    }

    IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else {
            Texture2D tex=((DownloadHandlerTexture)request.downloadHandler).texture;
            Sprite ss=Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
            map.sprite = ss;
            //loading.SetActive(false);

        }

    }
}
