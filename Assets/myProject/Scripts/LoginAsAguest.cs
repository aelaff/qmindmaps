﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoginAsAguest : MonoBehaviour
{
    public GameObject loadingPanel;
   public void SignInAsGuest()
    {
        loadingPanel.SetActive(true);
        StartCoroutine(RestClient.Instance.Login("guest@gmail.com", "123456", status =>
        {
            loadingPanel.SetActive(false);
           

            if (status.status)
            {
                SceneManager.LoadScene("Parts");
             
            }
        }
        ));

    }
}
