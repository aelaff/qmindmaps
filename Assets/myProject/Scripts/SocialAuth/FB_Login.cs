﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FB_Login : MonoBehaviour
{
    void Awake()
    {
        if (!FB.IsInitialized)
            FB.Init(InitCallback, OnHideUnity);
        else
            FB.ActivateApp();
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
            FB.ActivateApp();
        else
            Debug.Log("Failed to Initialize the Facebook SDK");
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;

    }

    public void FB_login()
    {
        var perms = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }
    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            var aToken = AccessToken.CurrentAccessToken;
            Debug.Log(aToken.TokenString);
            StartCoroutine(RestClient.Instance.LoginSocial(aToken.TokenString, "facebook", status =>
            {


                if (status.status)
                {

                    PlayerPrefs.SetString(Constants.Full_Name, status.items.user.name);

                    SceneManager.LoadScene("Parts");
                    Debug.Log("API " + status);


                }

            }
        ));

            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
            FirebaseManager.instance.FacebookAuth(aToken.TokenString);
            
        }
        else
        {
            Debug.Log("User cancelled login" + result.Error);
        }
    }

}
