﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Models.Maps;

[System.Serializable]
public class Models
{
    [System.Serializable]
    public class Token
    {
        public string token_type;
        public int expires_in;
        public string access_token;
        public string refresh_token;
    }

    [System.Serializable]
    public class Country
    {
        public int id;
        public string name;
    }

    [System.Serializable]
    public class User
    {
        public int id;
        public string name;
        public string email;
        public string mobile;
        public int age;
        public string gender;
        public Country country;
        public string memorization_level;
        public string birth_date;
        public int sound_on;
        public int alert_on;
        public int repeat_num;
        public Reciter reciter;
    }
    [System.Serializable]
    public class Reciter
    {
        public int id;
        public string name;
    }
    [System.Serializable]
    public class Items
    {
        public Token token;
        public User user;
    }

    [System.Serializable]
    public class RootUser
    {
        public bool status;
        public int statusCode;
        public string message;
        public Items items;
    }

    [System.Serializable]
    public class Item
    {
        public int id;
        public string name;
    }

    [System.Serializable]
    public class RootCountry
    {
        public bool status;
        public int statusCode;
        public string message;
        public List<Item> items;
    }

    [System.Serializable]
    public class Sura {
        [System.Serializable]
        public class Childe
        {
            public int id;
            public int number;
            public string name;
            public string image;
            public int form_verse;
            public int to_verse;
            public List<object> childes;
        }
        [System.Serializable]
        public class Sound
        {
            public int id;
            public string sound;
            public Reciter reciter;
        }
        [System.Serializable]
        public class Map
        {
            public int id;
            public int number;
            public string name;
            public string image;
            public int form_verse;
            public int to_verse;
            public List<Childe> childes;
        }
        [System.Serializable]
        public class Datum
        {
            public int id;
            public string name;
            public int verse_count;
            public string type;
            public int page;
            public string another_name;
            public int has_many_part;
            public List<Map> maps;
        }

        [System.Serializable]
        public class Items
        {
            public List<Datum> data;
            public int total_pages;
            public int current_page;
            public int total_records;
        }

        [System.Serializable]
        public class Root
        {
            public bool status;
            public int statusCode;
            public string message;
            public Items items;
        }


    }
    [System.Serializable]
    public class Reciters
    {
        public bool status;
        public int statusCode;
        public string message;
        public List<Reciter> items;
    }

    [System.Serializable]
    public class Maps {
        [System.Serializable]
        public class Surah
        {
            public int id;
            public string name;
        }

        [System.Serializable]
        public class Sub
        {
            public int id;
            public int number;
            public string image;
            public object parent_id;
            public string text;
            public List<object> childes;
        }

        [System.Serializable]
        public class Vers
        {
            public int id;
            public string text;
            public int number;
            public List<Sub> subs;
        }

        [System.Serializable]
        public class Items
        {
            public int id;
            public int number;
            public string name;
            public object parent_id;
            public string image;
            public string video;
            public string video2;
            public int form_verse;
            public int to_verse;
            public Surah surah;
            public List<Vers> verses;
            public List<Childe> childes;
        }
        [System.Serializable]
        public class Childe
        {
            public int id;
            public int number;
            public string name;
            public string image;
            public int form_verse;
            public int to_verse;
            public List<Vers> verses;
            public List<object> childes;
        }
        [System.Serializable]
        public class RootMap
        {
            public bool status;
            public int statusCode;
            public string message;
            public Items items;
        }
    }
    [System.Serializable]
    public class UpdatedUser {


        

        [System.Serializable]
        public class RootUpdatedUser
        {
            public bool status;
            public int statusCode;
            public string message;
            public User items;
        }

        public class Item
        {
            public string fieldname;
            public string message;
        }

        public class RootFaultUpdatedUser
        {
            public bool status;
            public int statusCode;
            public string message;
            public List<Item> items;
        }
    }

    [System.Serializable]
    public class AudioSura {
        [System.Serializable]
        public class Number
        {
            public int inQuran;
            public int inSurah;
        }

        [System.Serializable]
        public class Sajda
        {
            public bool recommended;
            public bool obligatory;
        }

        [System.Serializable]
        public class Meta
        {
            public int juz;
            public int page;
            public int manzil;
            public int ruku;
            public int hizbQuarter;
            public Sajda sajda;
        }

        [System.Serializable]
        public class Transliteration
        {
            public string en;
            public string id;
        }

        [System.Serializable]
        public class Text
        {
            public string arab;
            public Transliteration transliteration;
        }

        [System.Serializable]
        public class Translation
        {
            public string en;
            public string id;
        }

        [System.Serializable]
        public class Audio
        {
            public string primary;
            public List<string> secondary;
        }

        [System.Serializable]
        public class Id
        {
            public string @short;
            public string @long;
        }

        [System.Serializable]
        public class Tafsir
        {
            public Id id;
        }

        [System.Serializable]
        public class Name
        {
            public string @short;
            public string @long;
            public Transliteration transliteration;
            public Translation translation;
        }

        [System.Serializable]
        public class Revelation
        {
            public string arab;
            public string en;
            public string id;
        }

        [System.Serializable]
        public class PreBismillah
        {
            public Text text;
            public Translation translation;
            public Audio audio;
        }

        [System.Serializable]
        public class Surah
        {
            public int number;
            public int sequence;
            public int numberOfVerses;
            public Name name;
            public Revelation revelation;
            public Tafsir tafsir;
            public PreBismillah preBismillah;
        }

        [System.Serializable]
        public class Data
        {
            public Number number;
            public Meta meta;
            public Text text;
            public Translation translation;
            public Audio audio;
            public Tafsir tafsir;
            public Surah surah;
        }

        [System.Serializable]
        public class Root
        {
            public int code;
            public string status;
            public string message;
            public Data data;
        }

    }
    [System.Serializable]
    public class VerseItems
    {
        public int id;
        public string text;
        public Surah surah;
        public Sura.Map map;
        public int number;
        public List<Sub> subs;
        public List<Sura.Sound> sounds;
    }

    [System.Serializable]
    public class VerseRoot
    {
        public bool status;
        public int statusCode;
        public string message;
        public VerseItems items;
    }

}
