﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class SliderManager : MonoBehaviour
{
    
    public GameObject slide1,vid;
    
    private void Awake()
    {
        Invoke("DisableVidEnableSlide1", 15); 
    }
    
    void DisableVidEnableSlide1()
    {
            vid.SetActive(false);
            slide1.SetActive(true);
        
    }
}
