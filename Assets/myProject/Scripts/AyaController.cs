﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class AyaController : MonoBehaviour
{
    public Transform ayaSubsHolder,subElement;
    public RawImage map;
    private AudioClip _audio;
    public AudioSource audio;
    public void Setup(List<Models.Maps.Sub> subs,Color subjectColor) {
        DeleteChildsIfExist(ayaSubsHolder);
        for (int i = 0; i < subs.Count; i++)

        {
            Transform subElementOBJ = Instantiate(subElement, ayaSubsHolder);
            subElementOBJ.gameObject.SetActive(false);
            if (i == 0)
            {
                subElementOBJ.name = "first";
            }
            else if (i == subs.Count - 1)
            {
                subElementOBJ.name = "last";
            }

            subElementOBJ.GetComponent<SubElementController>().Setup(subs[i].text, subs[i].image, subjectColor);

        }
    }
    //IEnumerator SlowLoop(List<Models.Maps.Sub> subs)
    //{
         
    //        yield return StartCoroutine(DownloadImage(sub.image));
           
       
    //}
    //IEnumerator DownloadImage(string MediaUrl)
    //{
    //    UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
    //    yield return request.SendWebRequest();
    //    if (request.isNetworkError || request.isHttpError)
    //        Debug.Log(request.error);
    //    else
    //    {
    //        map.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
    //        //loading.SetActive(false);

    //    }
    //}
    void ShowHideSibblengs(Transform parent,bool show) {
        for (int i = 0; i < parent.childCount; i++)
        {
            if (name != parent.GetChild(i).name)
            {
                parent.GetChild(i).gameObject.SetActive(show);
                
            }
            bool hasCompo = parent.GetChild(i).TryGetComponent(out AudioSource audioSource);
            if (hasCompo)
                audioSource.enabled = !show;

        }
        //if (currShow) {
        //    for (int i = 0; i < parent.childCount; i++)
        //    {
        //        if (name != parent.GetChild(i).name)
        //        {
        //            bool hasCompo = parent.GetChild(i).TryGetComponent(out AudioSource audioSource);
        //            if (hasCompo)
        //                audioSource.enabled = false;
        //        }
        //    }
        //}
    }
    void DeleteChildsIfExist(Transform parentPanel)
    {
        for (int i = 0; i < parentPanel.childCount; i++)
        {
            Destroy(parentPanel.GetChild(i).gameObject);
        }
    }
    bool toggle = true;
    public void ShowSons() {
        ShowHideSibblengs(transform.parent,!toggle);
        ShowHideSibblengs(ayaSubsHolder,toggle);

        if (toggle) {
            LoadAudio();
        }
        toggle = !toggle;


    }
    public void LoadAudio() {

        //string[] ayaSura = name.Split('_');
        
            StartCoroutine(RestClient.Instance.GetVereseAudio(name, result =>
            {

                if (result.status)
                {
                    foreach (var sound in result.items.sounds)
                    {
                        StartCoroutine(LoadAudio(sound.sound));

                    }


                }
            }));

    }


    

    IEnumerator LoadAudio(string url)
    {
        if (audio.clip == null)
        {
            // Start a download of the given URL
            WWW www = new WWW(url);
            // Wait for download to complete
            yield return www;
            // get text
            _audio = www.GetAudioClip(false, false);
            audio.clip = _audio;
            // having audio here. Do with it whatever you want...
            audio.Play();
        }
        else {
            audio.Play();

        }
    }

    // ... for instance, use OnGUI to render it as a label:
    //void OnGUI()
    //{
    //    if (null != _audio)
    //    {
    //        if (!audio.isPlaying)
    //            audio.Play();
    //        //GUI.Box(new Rect(10, 10, 400, 300), "Play");


    //    }
    //    else
    //    { }
    //}


}
