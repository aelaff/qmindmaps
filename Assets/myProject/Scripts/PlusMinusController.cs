﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlusMinusController : MonoBehaviour
{
    public int max, min;
    public int currentVal;
    public Text valText;
    public Button plusBTN, minusBTN;

    private void Start()
    {
        currentVal = min;
        //minusBTN.interactable = false;
        valText.text = currentVal + "";

    }
    public void IncreaseVal() {
        minusBTN.interactable = true;
        plusBTN.interactable = true;
        if (currentVal < max)
        {
            valText.text = (++currentVal) + "";
        }
        if (currentVal == max)
            plusBTN.interactable = false;
        //minusBTN.interactable = true;

      
}
    public void DecreaseVal()
    {
        minusBTN.interactable = true;
        plusBTN.interactable = true;
        if (currentVal > min)
        {
            valText.text = (--currentVal) + "";
        }
        if(currentVal == min)
        {
            minusBTN.interactable = false;

        }

    }

}
