﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainAyaAudioLoader : MonoBehaviour
{
    public static AudioClip _audio;
    AudioSource audio;
    public bool isAya = false;
    private void Start()
    {
        audio = transform.parent.GetComponent<AudioSource>();
    }
    private void OnEnable()
    {
        //audio.clip = null;
    }
    public void LoadAudio()
    {
        Animator anim = GetComponent<Animator>();
        anim.Play("mapElement", -1, 0f);
        Debug.Log("Clicked");

        audio.clip = null;
        if (isAya) { 
        //string[] ayaSura = name.Split('_');

        StartCoroutine(RestClient.Instance.GetVereseAudio(name, result =>
        {

            if (result.status)
            {
                foreach (var sound in result.items.sounds)
                {
                    StartCoroutine(LoadAudio(sound.sound));

                }


            }
        }));
        }

    }
    IEnumerator LoadAudio(string url)
    {

        if (audio.clip == null)
        {
            // Start a download of the given URL
            WWW www = new WWW(url);
            // Wait for download to complete
            yield return www;
            // get text
            _audio = www.GetAudioClip(false, false);
            audio.clip = _audio;
            // having audio here. Do with it whatever you want...
            audio.Play();
        }
        else
        {
            audio.Play();

        }
    }
}
