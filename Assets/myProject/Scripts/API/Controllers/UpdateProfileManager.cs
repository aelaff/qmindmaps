﻿using ArabicSupport;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static Models;

public class UpdateProfileManager : MonoBehaviour
{
    public Text DOBTxt, mobileTxt,repeatingTxt;
    public GenderController gender,memoryDegree;
    public Dropdown country,sheikhReading;
    public Toggle notifications, sounds;
    public GameObject loadingPanel;
    public List<Item> Countries;
    private void Start()
    {

        SetAllData();


    }

    void SetAllData() {
        try
        {
            string age=PlayerPrefs.GetString("birth_date");
            string country_id= PlayerPrefs.GetString("country_id");
            string phone = PlayerPrefs.GetString("phone");
            string memorization_level = PlayerPrefs.GetString("memorization_level");
            string repeat_num = PlayerPrefs.GetString("repeat_num");
            string alert_on = PlayerPrefs.GetString("alert_on");
            string sound_on = PlayerPrefs.GetString("sound_on");
            string reciter_id = PlayerPrefs.GetString("reciter_id");

            loadingPanel.SetActive(true);
            StartCoroutine(RestClient.Instance.GetCountries(result =>
            {
                loadingPanel.SetActive(false);

                if (result.status)
                {
                    country.options.Clear();
                    //Debug.Log(result.items.Count);
                    Countries = result.items;
                    foreach (var item in result.items)
                    {
                        country.options.Add(new Dropdown.OptionData() { text = ArabicFixer.Fix(item.name) });
                    }
                }
            }));
            StartCoroutine(RestClient.Instance.GetReciters(result =>
            {
                loadingPanel.SetActive(false);

                if (result.status)
                {
                    sheikhReading.options.Clear();
                    //Debug.Log(result.items.Count);
                    foreach (var item in result.items)
                    {
                        sheikhReading.options.Add(new Dropdown.OptionData() { text = ArabicFixer.Fix(item.name) });
                    }
                }
            }));

            //setting data
            DOBTxt.text = age;
            mobileTxt.text = phone;
            repeatingTxt.text = repeat_num;
            sheikhReading.value = int.Parse(reciter_id);
            if (memorization_level == "new")
            {
                memoryDegree.SetEnabledBTN(0);
            }
            else
            {
                memoryDegree.SetEnabledBTN(1);
            }
            country.value = int.Parse(country_id);
            if (int.Parse(sound_on) == 1)
            {
                sounds.isOn = true;
            }
            else
            {
                sounds.isOn = false;

            }
            if (int.Parse(alert_on) == 1)
            {
                notifications.isOn = true;
            }
            else
            {
                notifications.isOn = false;

            }
        }
        catch (Exception)
        {

        }
    }
    public void UpdateProfile() {
        //if the profile completed
        string memo_level = "";
        if (memoryDegree.genderID == 0) {
            memo_level = "new";
        }
        else {
            memo_level = "keeper"; 
        }
        string soundsNum,alertNum;
        if (sounds.isOn)
        {
            soundsNum = "1";
        }
        else {
            soundsNum = "0";

        }
        if (notifications.isOn)
        {

            alertNum = "1";
        }
        else
        {
            alertNum = "0";

        }
        StartCoroutine(RestClient.Instance.UpdateProfile(DOBTxt.text, country.value + "", mobileTxt.text, memo_level, repeatingTxt.text, alertNum, soundsNum, sheikhReading.value + "", result =>
          {
              loadingPanel.SetActive(false);

              if (result.status)
              {
                  //Debug.Log(RestClient.Instance.user.items.user.name);
                  try
                  {
                      PlayerPrefs.SetInt(RestClient.Instance.user.items.user.email + Constants.ProfileCompleted, 1);
                  }
                  catch (Exception)
                  {

                  }
                  finally {
                      SceneManager.LoadScene("Parts");
                  }
                  
              }
          }));

        //PlayerPrefs.SetInt(Constants.ProfileCompleted, 1);
        //SceneManager.LoadScene("Parts");


    }

}
