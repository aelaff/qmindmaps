﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Firebase;
using Firebase.Auth;
using Google;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GoogleSignIn : MonoBehaviour
{
    string webClientId = "120497690452-58okgbfj9hqg44r272o1r58hikb6pmi7.apps.googleusercontent.com";

    private FirebaseAuth auth;
    private GoogleSignInConfiguration configuration;

    private void Awake()
    {
        configuration = new GoogleSignInConfiguration { WebClientId = webClientId, RequestEmail = true, RequestIdToken = true };
        //CheckFirebaseDependencies();
    }

    private void CheckFirebaseDependencies()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                if (task.Result == DependencyStatus.Available)
                    auth = FirebaseAuth.DefaultInstance;
                else
                    AddToInformation("Could not resolve all Firebase dependencies: " + task.Result.ToString());
            }
            else
            {
                AddToInformation("Dependency check was not completed. Error : " + task.Exception.Message);
            }
        });
    }

    public void SignInWithGoogle() { OnSignIn(); }
    public void SignOutFromGoogle() { OnSignOut(); }

    private void OnSignIn()
    {
        Google.GoogleSignIn.Configuration = configuration;
        Google.GoogleSignIn.Configuration.UseGameSignIn = false;
        Google.GoogleSignIn.Configuration.RequestIdToken = true;
        AddToInformation("Calling SignIn");

        Google.GoogleSignIn.DefaultInstance.SignIn().ContinueWith(this.OnAuthenticationFinished);
    }

    private void OnSignOut()
    {
        AddToInformation("Calling SignOut");
        Google.GoogleSignIn.DefaultInstance.SignOut();
    }

    public void OnDisconnect()
    {
        AddToInformation("Calling Disconnect");
        Google.GoogleSignIn.DefaultInstance.Disconnect();
    }

    internal void OnAuthenticationFinished(Task<GoogleSignInUser> task)
    {
        if (task.IsFaulted)
        {
            using (IEnumerator<Exception> enumerator = task.Exception.InnerExceptions.GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    Google.GoogleSignIn.SignInException error = (Google.GoogleSignIn.SignInException)enumerator.Current;
                    AddToInformation("Got Error: " + error.Status + " " + error.Message);
                }
                else
                {
                    AddToInformation("Got Unexpected Exception?!?" + task.Exception);
                }
            }
        }
        else if (task.IsCanceled)
        {
            AddToInformation("Canceled");
        }
        else
        {
            AddToInformation("Welcome: " + task.Result.DisplayName + "!");
            AddToInformation("Email = " + task.Result.Email);
            AddToInformation("Google ID Token = " + task.Result.IdToken);
            
            AddToInformation("Email = " + task.Result.Email);
            StartCoroutine(RestClient.Instance.LoginSocial(task.Result.IdToken, "google", status =>
            {
                

                if (status.status)
                {

                    PlayerPrefs.SetString(Constants.Full_Name, status.items.user.name);

                    SceneManager.LoadScene("Parts");
                    Debug.Log("API "+status);
                    

                }
                
            }
        ));
            SignInWithGoogleOnFirebase(task.Result.IdToken);
            //SceneManager.LoadScene("CompleteProfile");
        }
    }

    private void SignInWithGoogleOnFirebase(string idToken)
    {
        Credential credential = GoogleAuthProvider.GetCredential(idToken, null);

        auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
        {
            AggregateException ex = task.Exception;
            if (ex != null)
            {
                if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
                    AddToInformation("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
            }
            else
            {
                AddToInformation("Sign In Successful.");
            }
            
        });
    }

    public void OnSignInSilently()
    {
        Google.GoogleSignIn.Configuration = configuration;
        Google.GoogleSignIn.Configuration.UseGameSignIn = false;
        Google.GoogleSignIn.Configuration.RequestIdToken = true;
        AddToInformation("Calling SignIn Silently");

        Google.GoogleSignIn.DefaultInstance.SignInSilently().ContinueWith(this.OnAuthenticationFinished);
    }

    public void OnGamesSignIn()
    {
        Google.GoogleSignIn.Configuration = configuration;
        Google.GoogleSignIn.Configuration.UseGameSignIn = true;
        Google.GoogleSignIn.Configuration.RequestIdToken = false;

        AddToInformation("Calling Games SignIn");

        Google.GoogleSignIn.DefaultInstance.SignIn().ContinueWith(this.OnAuthenticationFinished);
    }

    private void AddToInformation(string str) => Debug.Log(str);
}