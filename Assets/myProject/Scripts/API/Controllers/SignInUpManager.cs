﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SignInUpManager : MonoBehaviour
{
    public InputField signInEmail, signInPassword, registerEmail, registerPassword, registerUsername,forgetPasswordEmail;
    public GameObject loadingPanel;
    public Text errorMessage;
    private void Start()
    {
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString(Constants.UserName)) && 
            !string.IsNullOrEmpty(PlayerPrefs.GetString(Constants.Password))) {
            DoSignIn(PlayerPrefs.GetString(Constants.UserName), PlayerPrefs.GetString(Constants.Password));
        }
    }
    void DoSignIn(string username, string password) {
        loadingPanel.SetActive(true);
        errorMessage.gameObject.SetActive(false);

        StartCoroutine(RestClient.Instance.Login(username, password, status =>
        {
            loadingPanel.SetActive(false);
            //errorMessage.GetComponent<ArabicText>().Text = status.message;
            //errorMessage.GetComponent<ArabicText>().Text = PlayerPrefs.GetString(Constants.UserName)
            //+" "+ PlayerPrefs.GetString(Constants.Password) ;

            if (status.status)
            {
                RestClient.Instance.user = status;

                if (PlayerPrefs.GetInt(username + Constants.ProfileCompleted) == 1)
                {
                    SceneManager.LoadScene("Parts");
                }
                else
                {
                    SceneManager.LoadScene("CompleteProfile");
                }

            }
            else {
                errorMessage.gameObject.SetActive(true);
                errorMessage.GetComponent<ArabicText>().Text = "خطأ في اسم المستخدم أو كلمة المرور";
            }
        }
        ));
    }
    public void SignIn() {
        if (string.IsNullOrEmpty(signInEmail.text))
        {
            errorMessage.GetComponent<ArabicText>().Text = "الرجاء إدخال البريد الالكتروني";
        }
        else if (string.IsNullOrEmpty(signInPassword.text))
        {
            errorMessage.GetComponent<ArabicText>().Text = "الرجاء إدخال كلمة المرور";
        }
        else if (string.IsNullOrEmpty(signInEmail.text) && string.IsNullOrEmpty(signInPassword.text))
        {
            errorMessage.GetComponent<ArabicText>().Text = "الرجاء إدخال البريد الالكتروني و كلمة المرور";
        }
        else {
            DoSignIn(signInEmail.text, signInPassword.text);
        }
    }
    public void Register() {
        if (string.IsNullOrEmpty(registerEmail.text))
        {
            errorMessage.GetComponent<ArabicText>().Text = "الرجاء إدخال البريد الالكتروني";
        }
        else if (string.IsNullOrEmpty(registerPassword.text))
        {
            errorMessage.GetComponent<ArabicText>().Text = "الرجاء إدخال كلمة المرور";
        }
        else if (string.IsNullOrEmpty(registerEmail.text) && string.IsNullOrEmpty(registerPassword.text))
        {
            errorMessage.GetComponent<ArabicText>().Text = "الرجاء إدخال البريد الالكتروني و كلمة المرور";
        }
        else
        {
            loadingPanel.SetActive(true);
            StartCoroutine(RestClient.Instance.SignUp(registerUsername.text,registerEmail.text, registerPassword.text, status =>
            {
                loadingPanel.SetActive(false);
                errorMessage.GetComponent<ArabicText>().Text = status.message;
                errorMessage.gameObject.SetActive(true);
                if (status.status)
                {
                    DoSignIn(registerEmail.text, registerPassword.text);

                }
            }
            ));
        }
    }
    public void ForgetPassowrd() {
        if (string.IsNullOrEmpty(forgetPasswordEmail.text))
        {
            errorMessage.gameObject.SetActive(true);
            errorMessage.GetComponent<ArabicText>().Text = "الرجاء إدخال البريد الالكتروني";
        }
        else {
            //loadingPanel.SetActive(true);
            StartCoroutine(RestClient.Instance.ForgetPassword(forgetPasswordEmail.text, status =>
            {
               // loadingPanel.SetActive(false);
                errorMessage.gameObject.SetActive(true);

                errorMessage.GetComponent<ArabicText>().Text = status.message;
                //if (status.status)
                //{
                    

                //}
            }
            ));
        }
    }
}
