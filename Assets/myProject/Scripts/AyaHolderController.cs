﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AyaHolderController : MonoBehaviour
{
    public Image shape;
    public Sprite[] allShapes;
    public ArabicText ayaNumber;

    public void Setup(string ayaNo, Color subjectColor)
    {
        
        if (name == "first")
        {
            shape.sprite = allShapes[0];
        }
        else if (name == "last")
        {
            shape.sprite = allShapes[1];

        }
        else
        {
            shape.sprite = allShapes[2];

        }
        shape.color = subjectColor;
        ayaNumber.Text = ayaNo;
        ayaNumber.gameObject.GetComponent<Text>().color = new Color(subjectColor.g, subjectColor.b,subjectColor.r,1);

    }
}
