﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour
{
    //public const string API_Root = "http://mind-mapping.nstechsdev.com/api/";
    public const string API_Root = "https://mindmap4quran.com/public/api/";
    public const string API_Login = "login";
    public const string API_Signup = "sign-up";
    public const string API_ForgetPassword = "forget";
    public const string API_UpdateProfile = "user";
    public const string API_Countries = "countries";
    public const string API_Get_Suras = "suras";
    public const string API_Get_Reciters = "reciters";
    public const string API_Get_Maps = "map";
    public const string API_Get_Verse = "verse";
    public const string API_Logout = "logout";
    public const string Access_Token = "access_token";
    public const string UserName = "username";
    public const string Password = "password";
    public const string Full_Name = "fullName";
    public const string SliderShown = "SliderShown";

    public const string Client_Id = "94631e2e-40d8-4af4-8cdb-6e1e251210c9";
    public const string Client_Secret = "sku0HdLFngfF2J9IONm87wLHNUklpo5RDKY5zV77";



    public const string ProfileCompleted = "ProfileCompleted";
    public const string MapLink = "map_link";
    public const string SuraTitle = "sura_title";


}
