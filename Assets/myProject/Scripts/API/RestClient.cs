﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using static Models;
using static Models.Maps;
using static Models.Sura;

public class RestClient : MonoBehaviour
{
    private static RestClient instance;
    
    public RootUser user;
    public string fromWhereComing = "";
    public int partID ;
    
    public static RestClient Instance
    {

        get
        {
            //RestConsumer.instance.userData.user.message = "Hello I am Ahmed";

            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<RestClient>();

                if (instance == null)
                {
                    GameObject container = new GameObject("RestClient");
                    instance = container.AddComponent<RestClient>();
                    DontDestroyOnLoad(container);
                }
            }
            //Debug.Log("Email : "+instance.user.items.user.email);
            return instance;
        }
    }
    public static RootMap currentMap;
    public List<Datum> AllSuras;
    private void Start()
    {
        instance = this;
        AllSuras = new List<Datum>();
        StartCoroutine(RestClient.Instance.GetAllSuras(result =>
        {

            if (result.status)
            {
                AllSuras = result.items.data;
                
            }
        }));

    }
    public IEnumerator Login(string credential, string password, Action<RootUser> callback)
    {

        WWWForm form = new WWWForm();
        form.AddField("grant_type", "password");
        form.AddField("username", credential);
        form.AddField("password", password);
        form.AddField("client_id", Constants.Client_Id);
        form.AddField("client_secret", Constants.Client_Secret);
        form.AddField("device_id", SystemInfo.deviceUniqueIdentifier);
#if UNITY_IPHONE
            form.AddField("device_type", "ios");
#endif

#if UNITY_ANDROID
        form.AddField("device_type", "android");
#endif

        using (UnityWebRequest www = UnityWebRequest.Post(Constants.API_Root + Constants.API_Login, form))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    RootUser user = JsonUtility.FromJson<RootUser>(jsonResult);
                    if(user.status)
                        SaveAllUserData(
                            user.items.token.access_token,
                            user.items.user.name,
                            user.items.user.email,
                            password,
                            user.items.user.age + "",
                            user.items.user.country.id + "",
                            user.items.user.mobile,
                            user.items.user.memorization_level,
                            user.items.user.repeat_num + "",
                            user.items.user.alert_on + "",
                            user.items.user.sound_on + "",
                            user.items.user.reciter.id + "");
                    //instance.user = user;


                    callback(user);
                }

            }
        }
    }
    public IEnumerator LoginSocial(string access_token, string provider, Action<RootUser> callback)
    {

        WWWForm form = new WWWForm();
        form.AddField("grant_type", "social");
        form.AddField("access_token", access_token);
        form.AddField("provider", provider);
        form.AddField("client_id", Constants.Client_Id);
        form.AddField("client_secret", Constants.Client_Secret);
        form.AddField("device_id", SystemInfo.deviceUniqueIdentifier);
#if UNITY_IPHONE
            form.AddField("device_type", "ios");
#endif

#if UNITY_ANDROID
        form.AddField("device_type", "android");
#endif

        using (UnityWebRequest www = UnityWebRequest.Post(Constants.API_Root + Constants.API_Login, form))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    RootUser user = JsonUtility.FromJson<RootUser>(jsonResult);
                    //PlayerPrefs.SetString(Constants.Access_Token, user.items.token.access_token);
                    //if (user.success)
                    //    FirebaseManager.instance.LoginEmailAuth(credential, credential);
                    instance.user = user;
                    SaveAllUserData(
                        user.items.token.access_token,
                        user.items.user.name,
                        user.items.user.email,
                        null,
                        user.items.user.age + "",
                        user.items.user.country.id + "",
                        user.items.user.mobile,
                        user.items.user.memorization_level,
                        user.items.user.repeat_num + "",
                        user.items.user.alert_on + "",
                        user.items.user.sound_on + "",
                        user.items.user.reciter.id + "");
                    callback(user);
                }

            }
        }
    }
    
    public IEnumerator SignUp(string user_name, string email, string password, Action<RootUser> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", user_name);
        form.AddField("email", email);
        form.AddField("password", password);
        form.AddField("memorization_level", "new");
        form.AddField("client_id", Constants.Client_Id);
        form.AddField("client_secret", Constants.Client_Secret);
        form.AddField("grant_type", "password");

        using (UnityWebRequest www = UnityWebRequest.Post(Constants.API_Root + Constants.API_Signup, form))
        {

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    RootUser user = JsonUtility.FromJson<RootUser>(jsonResult);
                    SaveAllUserData(
                        user.items.token.access_token,
                        user.items.user.name,
                        email,
                        password,
                        user.items.user.age + "",
                        user.items.user.country.id + "",
                        user.items.user.mobile,
                        user.items.user.memorization_level,
                        user.items.user.repeat_num + "",
                        user.items.user.alert_on + "",
                        user.items.user.sound_on + "",
                        user.items.user.reciter.id + "");
                    instance.user = user;
                    PlayerPrefs.SetString(Constants.Access_Token, user.items.token.access_token);

                    callback(user);


                }
            }
        }
    }
    public IEnumerator ForgetPassword(string email, Action<RootUser> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("email", email);
        form.AddField("client_id", Constants.Client_Id);
        form.AddField("client_secret", Constants.Client_Secret);

        using (UnityWebRequest www = UnityWebRequest.Post(Constants.API_Root + Constants.API_ForgetPassword, form))
        {

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    RootUser user = JsonUtility.FromJson<RootUser>(jsonResult);
                    callback(user);


                }
            }
        }
    }
    public IEnumerator UpdateProfile(string birth_date,string country_id,string phone,string memorization_level,string repeat_num,string alert_on,string sound_on,string reciter_id, Action<UpdatedUser.RootUpdatedUser> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("age", birth_date);
        form.AddField("country_id", country_id);
        form.AddField("reciter_id", 1);
        form.AddField("sound_on", sound_on);
        form.AddField("alert_on", alert_on);
        form.AddField("repeat_num", repeat_num);
        form.AddField("phone", phone);
        form.AddField("memorization_level", memorization_level);
        form.AddField("client_id", Constants.Client_Id);
        form.AddField("client_secret", Constants.Client_Secret);

        using (UnityWebRequest www = UnityWebRequest.Post(Constants.API_Root + Constants.API_UpdateProfile, form))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString(Constants.Access_Token));
            www.SetRequestHeader("X-HTTP-Method-Override", "PUT");

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    UpdatedUser.RootUpdatedUser user = JsonUtility.FromJson<UpdatedUser.RootUpdatedUser>(jsonResult);
                    SaveAllUserData(
                        PlayerPrefs.GetString(Constants.Access_Token),
                        user.items.name,
                        user.items.email,
                        null,
                        user.items.age + "",
                        user.items.country.id + "",
                        user.items.mobile,
                        user.items.memorization_level,
                        user.items.repeat_num + "",
                        user.items.alert_on + "",
                        user.items.sound_on + "",
                        user.items.reciter.id + "");
                    callback(user);

                }
            }
        }
    }
    public IEnumerator GetCountries(Action<RootCountry> callback)
    {


        using (UnityWebRequest www = UnityWebRequest.Get(Constants.API_Root + Constants.API_Countries))
        {

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    RootCountry countries = JsonUtility.FromJson<RootCountry>(jsonResult);
                    callback(countries);


                }
            }
        }
    }
    public IEnumerator GetReciters(Action<Reciters> callback)
    {


        using (UnityWebRequest www = UnityWebRequest.Get(Constants.API_Root + Constants.API_Get_Reciters))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString(Constants.Access_Token));

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    Reciters reciters = JsonUtility.FromJson<Reciters>(jsonResult);
                    callback(reciters);


                }
            }
        }
    }
    public IEnumerator GetSuras(string sura_name, Action<Sura.Root> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", sura_name);

        using (UnityWebRequest www = UnityWebRequest.Post(Constants.API_Root + Constants.API_Get_Suras, form))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString(Constants.Access_Token));

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    Sura.Root sura = JsonUtility.FromJson<Sura.Root>(jsonResult);

                    callback(sura);


                }
            }
        }
    }
    public IEnumerator GetAllSuras(Action<Sura.Root> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("page_size", 114);
        form.AddField("page_number", 1);

        using (UnityWebRequest www = UnityWebRequest.Post(Constants.API_Root + Constants.API_Get_Suras, form))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString(Constants.Access_Token));

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    Sura.Root sura = JsonUtility.FromJson<Sura.Root>(jsonResult);

                    callback(sura);


                }
            }
        }
    }
    public IEnumerator GetSuraAudio(string suraID,string ayaID,Action<AudioSura.Root> callback)
    {


        using (UnityWebRequest www = UnityWebRequest.Get("https://api.quran.sutanlab.id/surah/" +suraID+"/"+ayaID))
        {

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    AudioSura.Root sura = JsonUtility.FromJson<AudioSura.Root>(jsonResult);
                    
                    callback(sura);


                }
            }
        }
    }
    public IEnumerator GetVereseAudio(string verseID, Action<VerseRoot> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("reciter_id", PlayerPrefs.GetInt("sheikhID",1) +"");
        using (UnityWebRequest www = UnityWebRequest.Post(Constants.API_Root + Constants.API_Get_Verse + "/" + verseID, form))
        {
            
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString(Constants.Access_Token));

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    VerseRoot verse = JsonUtility.FromJson<VerseRoot>(jsonResult);

                    callback(verse);


                }
            }
        }
    }
    public IEnumerator GetMaps(string mapID, Action<Maps.RootMap> callback)
    {

        using (UnityWebRequest www = UnityWebRequest.Get(Constants.API_Root + Constants.API_Get_Maps+"/"+mapID))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString(Constants.Access_Token));

            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    Maps.RootMap map = JsonUtility.FromJson<Maps.RootMap>(jsonResult);

                    callback(map);


                }
            }
        }
    }
    public IEnumerator Logout( Action<RootUser> callback)
    {

        WWWForm form = new WWWForm();


        using (UnityWebRequest www = UnityWebRequest.Post(Constants.API_Root + Constants.API_Logout, form))
        {
            www.SetRequestHeader("Authorization", "Bearer " + PlayerPrefs.GetString(Constants.Access_Token));
            yield return www.SendWebRequest();
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    RootUser user = JsonUtility.FromJson<RootUser>(jsonResult);

                    callback(user);
                    DeleteAllUserData();
                }

            }
        }
    }
    void SaveAllUserData(string accessToken, string fullName, string email, string password, string birth_date, string country_id, string phone, string memorization_level, string repeat_num, string alert_on, string sound_on, string reciter_id)
    {
        PlayerPrefs.SetString(Constants.Access_Token, accessToken);
        PlayerPrefs.SetString(Constants.UserName, email);
        PlayerPrefs.SetString(Constants.Password, password);
        PlayerPrefs.SetString(Constants.Full_Name, fullName);
        PlayerPrefs.SetString("birth_date", birth_date);
        PlayerPrefs.SetString("country_id", country_id);
        PlayerPrefs.SetString("phone", phone);
        PlayerPrefs.SetString("memorization_level", memorization_level);
        PlayerPrefs.SetString("repeat_num", repeat_num);
        PlayerPrefs.SetString("alert_on", alert_on);
        PlayerPrefs.SetString("sound_on", sound_on);
        PlayerPrefs.SetString("reciter_id", reciter_id);
    }
    public void DeleteAllUserData()
    {
        Debug.Log("Deleted");
        PlayerPrefs.DeleteKey(Constants.Access_Token);
        PlayerPrefs.DeleteKey(Constants.UserName);
        PlayerPrefs.DeleteKey(Constants.Password);
        PlayerPrefs.DeleteKey(Constants.Full_Name);
        PlayerPrefs.DeleteKey("birth_date");
        PlayerPrefs.DeleteKey("country_id");
        PlayerPrefs.DeleteKey("memorization_level");
        PlayerPrefs.DeleteKey("repeat_num");
        PlayerPrefs.DeleteKey("alert_on");
        PlayerPrefs.DeleteKey("sound_on");
        PlayerPrefs.DeleteKey("reciter_id");

    }
}
