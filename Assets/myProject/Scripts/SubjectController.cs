﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Models.Maps;

public class SubjectController : MonoBehaviour
{
    bool isClicked = false;

    Transform mainLine;
    Vector3 StartPosition;
    public GameObject Aya;
    public Color subjectColor;
    public Transform AyatContainer;
    private GameObject subjectOBJ;
    // Start is called before the first frame update
    void Start()
    {
       // transform.GetChild(0).GetChild(0).GetComponent<Button>().onClick.AddListener(SubjectClicked);
       // transform.GetChild(0).GetChild(0).GetComponent<Image>().color = subjectColor;
       // Color newColor = new Color(subjectColor.b-0.3f, subjectColor.r - 0.1f, subjectColor.g - 0.1f, 1);
       //transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().color = newColor;
    }
    void AnimateAya(Transform button) {
        button.GetComponent<Animator>().Play("mapElement", 0);
        Debug.Log("Clicked");
    }
    void SubjectClicked()
    {
        if (!GetComponent<MainAyaAudioLoader>().isAya)
        {
            Transform button = transform.GetChild(0).GetChild(0);
            button.GetComponent<Button>().onClick.AddListener(delegate { AnimateAya(button); });  
            //disapear all subjects
            for (int i = 1; i < TreeCreator.allMapsList.Count; i++)
            {

                TreeCreator.allMapsList[i].transform.GetChild(0).gameObject.SetActive(false);
            }

            CopyClickedObjectProperties();
            //just show our subject
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(0).GetChild(1).gameObject.SetActive(true);

            StartPosition = transform.GetChild(0).transform.position;
        
                LeanTween.followLinear(TreeCreator.allMapsList[0].transform, MapController.instance.rootFollower, LeanProp.position, 3200f).
                    setEase(LeanTweenType.easeOutBack);

                LeanTween.followLinear(transform, MapController.instance.follower,LeanProp.position,2500f).
                    setEase(LeanTweenType.easeOutBack)
                    .setDelay(.5f);
                    transform.eulerAngles = Vector3.zero;
                //transform.GetChild(0).GetChild(0).eulerAngles = Vector3.zero;

                FindSubjectAndDrawAyat();
        }
        

    }

    private void CopyClickedObjectProperties()
    {
        UIEvents ue = GameObject.Find("Scripts").GetComponent<UIEvents>();
        subjectOBJ = ue.subjectGO;
        transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = subjectOBJ.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite;
        transform.GetChild(0).localPosition = subjectOBJ.transform.GetChild(0).localPosition;
        //transform.GetChild(0).GetChild(0).GetChild(0).localPosition = subjectOBJ.transform.GetChild(0).GetChild(0).localPosition;
        RectTransform uitransform = transform.GetChild(0).GetComponent<RectTransform>();
        uitransform.anchorMin = new Vector2(0f, 1f);
        uitransform.anchorMax = new Vector2(0f, 1f);
        uitransform.pivot = new Vector2(0.5f, 0.5f);

        RectTransform uitransform2 = transform.GetChild(0).GetChild(0).GetComponent<RectTransform>();
        uitransform2.pivot = new Vector2(0f, 0.5f);

        RectTransform uitransform3 = transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>();
        uitransform3.anchorMin = new Vector2(0.5f, 1f);
        uitransform3.anchorMax = new Vector2(0.5f, 1f);
        uitransform3.pivot = new Vector2(0.5f, 0f);


        transform.GetChild(0).GetChild(0).GetChild(1).gameObject.SetActive(false);

    }
   
    bool toggle = true;
    Childe clickedSubject = null;
    void FindSubjectAndDrawAyat() {

        AyatContainer.gameObject.SetActive(true);
        DeleteChildsIfExist(AyatContainer, 0);

        
        if (toggle) { 
            foreach (var subject in RestClient.currentMap.items.childes)
            {
                if (name == subject.id.ToString())
                {
                    clickedSubject = subject;
                }
            }
            toggle = false;
        }
            for (int i = 0; i < clickedSubject.verses.Count; i++)
            
        {
            Transform ayaOBJ = Instantiate(Aya, AyatContainer).transform;
            if (i == 0) {
                ayaOBJ.GetChild(0).name = "first";
            } else if (i == clickedSubject.verses.Count-1) {
                ayaOBJ.GetChild(0).name = "last";

            }
            ayaOBJ.GetChild(0).GetComponent<AyaHolderController>().Setup(clickedSubject.verses[i].number + "", subjectColor);

            //ayaOBJ.GetChild(0).GetChild(0).GetComponent<ArabicText>().Text = vers.number + "";
            //ayaOBJ.GetChild(0).GetChild(0).GetComponent<Text>().color = subjectColor;
            //ayaOBJ.GetChild(0).GetComponent<Image>().color = subjectColor;
            ayaOBJ.name = clickedSubject.verses[i].id + "";
            //LeanTween.followLinear(AyatContainer, MapController.instance.ayatFollower, LeanProp.position, 2500f).
            //setEase(LeanTweenType.easeOutBack)
            // .setDelay(1f);
            ayaOBJ.GetComponent<AyaController>().Setup(clickedSubject.verses[i].subs, subjectColor);

        }


    }
    void DeleteChildsIfExist(Transform parent,int start) {
        for (int i = start; i < parent.childCount; i++)
        {
            Destroy(parent.GetChild(i).gameObject);
        }
    }
}
