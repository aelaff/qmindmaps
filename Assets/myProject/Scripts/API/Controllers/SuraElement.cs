﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuraElement : MonoBehaviour
{
    public ArabicText suraName, suraType, noOfAyat;
    public void SuraSetup(string suraName, string suraType, string noOfAyat) {
        this.suraName.Text = suraName;
        this.suraType.Text = suraType;
        this.noOfAyat.Text =" عدد الآيات : " +noOfAyat;
    }
    public void SetColor(Color color)
    {
        GetComponent<Image>().color = color;
        this.suraName.gameObject.GetComponent<Text>().color = color;
        this.suraType.gameObject.GetComponent<Text>().color = color;
        this.noOfAyat.gameObject.GetComponent<Text>().color = color;
    }
    public void ActivateAll() {
        Debug.Log(name +" Active");
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
    }
    public void DeActivateAll()
    {
        Debug.Log(name +" DeActive");
        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).gameObject.SetActive(false);
        
    }
}
