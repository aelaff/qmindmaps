﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static Models.Maps;

[System.Serializable]
public class TreeCreator : MonoBehaviour
{
    public GameObject[] maps;
    public static List<GameObject> allMapsList;
    public Color[] myColors;
    GameObject mapRoot;
    public Vector3 mapsLocalPositions;
    
    public List<MapList> mapShapes;
    public string[] welcomeMsgs;
    public ArabicText welcomeMessageTxt;
    Image souraTypeImg;
    public Sprite level2, level3,makiya,madanya;



    // Start is called before the first frame update
    void Start()
    {
        allMapsList = new List<GameObject>();
        welcomeMessageTxt.Text = welcomeMsgs[Random.Range(0, welcomeMsgs.Length - 1)];
        welcomeMessageTxt.transform.parent.gameObject.SetActive(true);
        Invoke("HideMessage",3);
        mapRoot = Instantiate(maps[1]);
        mapRoot.transform.SetParent(transform);
        mapRoot.transform.localPosition = mapsLocalPositions;

        int souraRank = PlayerPrefs.GetInt("suraID");
        int souraAyat = 0;
        string souraType = "";
        if(RestClient.Instance.AllSuras!=null)
        foreach (var sura in RestClient.Instance.AllSuras)
        {
            if (sura.id == PlayerPrefs.GetInt("suraID")) {
                
                souraAyat = sura.verse_count;
                souraType = sura.type;
            }
        }

        mapRoot.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<ArabicText>().Text = "ترتيبها : " + souraRank;
        mapRoot.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<ArabicText>().Text = "آياتها : " +souraAyat;
        souraTypeImg = mapRoot.transform.GetChild(0).GetChild(3).GetComponent<Image>();
        if (souraType=="مكية") {
            souraTypeImg.sprite = makiya;
        }
        else {
            souraTypeImg.sprite = madanya;

        }

        mapRoot.transform.GetChild(0).GetChild(0).GetComponent<ArabicText>().Text = "سورة " + PlayerPrefs.GetString(Constants.SuraTitle);

        allMapsList.Add(mapRoot);
        List<Childe> subjects = RestClient.currentMap.items.childes;
        if (subjects.Count > 0)
            DrawSubjects(subjects);
        else
            DrawAyat(RestClient.currentMap.items.verses,"");


    }
    void HideMessage() {
        welcomeMessageTxt.transform.parent.gameObject.SetActive(false);
    }
    void DrawAyat(List<Vers> ayatSubject,string subjectName) {
        StartCoroutine(SlowLoop(ayatSubject,subjectName));
        
    }
    void DrawSubjects(List<Childe> ayatSubject) {
        //float angle = 90;
        for (int i = 0; i < ayatSubject.Count; i++)
        {
            GameObject map = Instantiate(mapShapes[ayatSubject.Count].list[i]);
            map.transform.SetParent(transform);
            map.transform.localPosition = mapsLocalPositions;
            //map.GetComponent<SubjectController>().subjectColor =myColors[i];
            Transform branch = map.transform.GetChild(0).GetChild(0);
            branch.GetChild(0).GetComponent<ArabicText>().Text = ayatSubject[i].name +"\n الآيات : "+ ayatSubject[i].form_verse+"-"+ ayatSubject[i].to_verse;
            map.name = ayatSubject[i].id+"";
            allMapsList.Add(map);
            List<Vers> branchVerses = ayatSubject[i].verses;
            string subjectName = ayatSubject[i].name;
            branch.GetComponent<Button>().onClick.AddListener(() => DrawAyatLevel2(branchVerses, subjectName) );
            branch.GetComponent<Image>().color = myColors[i];
            Color newColor = new Color(myColors[i].b-0.3f, myColors[i].r - 0.1f, myColors[i].g - 0.1f, 1);
            branch.GetChild(0).GetComponent<Text>().color = newColor;
        }
        allMapsList[0].transform.SetAsLastSibling();

    }
    void DrawAyatLevel2(List<Vers> branchVerses,string subjectName) {
        mapRoot.transform.GetChild(0).GetChild(0).GetComponent<ArabicText>().Text ="سورة "+ PlayerPrefs.GetString(Constants.SuraTitle)+"\n'"+subjectName+"'";
        mapRoot.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
        mapRoot.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
        mapRoot.GetComponent<Image>().sprite = level2;
        mapRoot.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);
        for (int i = 1; i < allMapsList.Count; i++)
        {
            Destroy(allMapsList[i].gameObject);
        }

        DrawAyat(branchVerses,subjectName);
    }

    IEnumerator SlowLoop(List<Vers> ayatSubject,string subjectName)
    {

        for (int i = 0; i < ayatSubject.Count; i++)
        {
            
            GameObject map = Instantiate(mapShapes[ayatSubject.Count].list[i]);
            map.transform.SetParent(transform);
            map.transform.localPosition = mapsLocalPositions;
            //map.GetComponent<SubjectController>().subjectColor = myColors[i];
            Transform branch = map.transform.GetChild(0).GetChild(0);
            branch.GetComponent<Image>().color = myColors[i];
            List<Sub> subs = ayatSubject[i].subs;
            string ayaTxt = "";
            if (!string.IsNullOrEmpty(subjectName))
            {
                ayaTxt = "'" + subjectName + "'\n آية رقم : " + ayatSubject[i].number;
            }
            else
            {
                ayaTxt = ayatSubject[i].text;
            }
            if (subs.Count > 1)
            {
                //branch.GetComponent<Button>().onClick.AddListener(() => FindSubjectAndDrawAyat(map, subs));                
                branch.GetComponent<Button>().onClick.AddListener(() => StartCoroutine(DrawAyatLevel3(ayaTxt,subs)));
            }
            Color newColor = new Color(myColors[i].b - 0.3f, myColors[i].r - 0.1f, myColors[i].g - 0.1f, 1);
            branch.GetChild(0).GetComponent<Text>().color = newColor;
            branch.GetChild(0).GetComponent<ArabicText>().Text =")"+(i+1)+"( " + ayatSubject[i].text;
            Image img = branch.GetChild(1).GetComponent<Image>();
            if (ayatSubject[i].subs.Count > 0)
                yield return StartCoroutine(DownloadImage(ayatSubject[i].subs[0].image, img));
            else
                img.color = new Color(0, 0, 0, 0);
            allMapsList[0].transform.SetAsLastSibling();
            map.GetComponent<MainAyaAudioLoader>().isAya = true;
            map.name = ayatSubject[i].id+"";
            allMapsList.Add(map);

        }

    }
    IEnumerator DrawAyatLevel3(string ayaTxt,List<Sub> ayatSubs) {
        mapRoot.transform.GetChild(0).GetChild(0).GetComponent<ArabicText>().Text = ayaTxt;
        mapRoot.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
        mapRoot.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
        mapRoot.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);

        for (int i = 1; i < allMapsList.Count; i++)
        {
            Destroy(allMapsList[i].gameObject);
        }
        for (int i = 0; i < ayatSubs.Count; i++)
        {

            GameObject map = Instantiate(mapShapes[ayatSubs.Count].list[i]);
            map.transform.SetParent(transform);
            map.transform.localPosition = mapsLocalPositions;
            //map.GetComponent<SubjectController>().subjectColor = myColors[i];
            Transform branch = map.transform.GetChild(0).GetChild(0);
            branch.GetComponent<Image>().color = myColors[i];

            Color newColor = new Color(myColors[i].b - 0.3f, myColors[i].r - 0.1f, myColors[i].g - 0.1f, 1);
            branch.GetChild(0).GetComponent<Text>().color = newColor;
            branch.GetChild(0).GetComponent<ArabicText>().Text = ayatSubs[i].text;
            Image img = branch.GetChild(1).GetComponent<Image>();
            yield return StartCoroutine(DownloadImage(ayatSubs[i].image, img));
            //Image img = branch.GetChild(1).GetComponent<Image>();
            //if (ayatSubject[i].subs.Count > 0)
            //    
            //else
            //    img.color = new Color(0, 0, 0, 0);
            allMapsList[0].transform.SetAsLastSibling();
            //map.GetComponent<MainAyaAudioLoader>().isAya = true;
            map.name = ayatSubs[i].id + "";

        }
    }
    IEnumerator DownloadImage(string MediaUrl,Image map)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            Texture2D tex = ((DownloadHandlerTexture)request.downloadHandler).texture;
            Sprite ss = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2());
            map.sprite = ss;

        }
    }


    //void FindSubjectAndDrawAyat(GameObject map,List<Sub> subs)
    //{
    //    Transform AyatContainer = map.GetComponent<SubjectController>().AyatContainer;
    //    GameObject Aya = maps[2];
    //    DeleteChildsIfExist(AyatContainer);
    //    for (int i = 0; i < subs.Count; i++)

    //    {
    //        Transform subElementOBJ = Instantiate(Aya, AyatContainer).transform;
    //        subElementOBJ.gameObject.SetActive(false);
    //        if (i == 0)
    //        {
    //            subElementOBJ.name = "first";
    //        }
    //        else if (i == subs.Count - 1)
    //        {
    //            subElementOBJ.name = "last";
    //        }

    //        subElementOBJ.GetComponent<SubElementController>().Setup(subs[i].text, subs[i].image, Color.red);

    //    }


    //}

    void DeleteChildsIfExist(Transform parent)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Destroy(parent.GetChild(i).gameObject);
        }
    }
}

[System.Serializable]
public class MapList
{
    public List<GameObject> list;
}