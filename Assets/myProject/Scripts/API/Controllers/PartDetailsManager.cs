﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PartDetailsManager : MonoBehaviour
{
    public ArabicText ayatTxt;
    public GenderController videoType;
    string mem_vid, tafs_vid,suraID;
    public List<AudioClip> clips;
    public AudioSource audioSource;
    public GameObject loading;
    public Slider audioDownloading;
    public Button audioBTN;
    public Sprite[] audioPlayStop;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        clips = new List<AudioClip>();
        

    }
    public void SetDetails(string ayat,string mem_vid,string tafs_vid,string img,string sura_title,int suraID,List<int> ayatNumbers) {
        ayatTxt.Text = ayat;
        this.mem_vid = mem_vid;
        this.tafs_vid = tafs_vid;
        this.suraID = suraID+"";
        PlayerPrefs.SetString(Constants.MapLink, img);
        PlayerPrefs.SetString(Constants.SuraTitle, sura_title);
        PlayerPrefs.SetInt("suraID", suraID);
       StartCoroutine(LoadAudioSources(ayatNumbers));

    }
    public void RunVideo() {
        if (videoType.genderID == 0)
        {
            Application.OpenURL(mem_vid);

        }
        else {
            Application.OpenURL(tafs_vid);

        }
    }
    bool isRun = false;
    public void RunAudio() {
        isRun = !isRun;
        if (isRun) {
            StartCoroutine(RunAudiosRepeatings());
            audioBTN.GetComponent<Image>().sprite = audioPlayStop[1];

        }
        else
        {
            StopAllCoroutines();
            audioBTN.GetComponent<Image>().sprite = audioPlayStop[0];

        }


    }
    IEnumerator RunAudiosRepeatings() {
        for (int i = 0; i < PlayerPrefs.GetInt("repeating"); i++)
        {
            //Debug.Log(i + "r");

            yield return StartCoroutine(runAudios());
        }
    }
    IEnumerator runAudios()
    {
        
        foreach (var clip in clips)
        {
            audioSource.clip = clip;
            audioSource.Play();
            yield return new WaitForSeconds(audioSource.clip.length);
        }

    }
    int countIndex = 0;
    IEnumerator LoadAudio(string url,string ayaNumber)
    {
        Debug.Log(url);
        WWW www = new WWW(url);
        // Wait for download to complete
        yield return www;
        // get text

        AudioClip _audio = www.GetAudioClip(true, true, AudioType.MPEG);
        _audio.name = countIndex + ".mp3";
        countIndex++;
        clips.Add(_audio);
        //using (UnityWebRequest www = UnityWebRequest.Get(url))
        //{
        //    yield return www.Send();
        //    if (www.isNetworkError || www.isHttpError)
        //    {
        //        Debug.Log(www.error);
        //    }
        //    else
        //    {
        //        string savePath = string.Format("{0}/{1}.wav", "D:", ayaNumber);
        //        System.IO.File.WriteAllText(savePath, www.downloadHandler.text);
        //        Debug.Log(savePath);
        //    }
        //}




    }
    //void ChangeAudioClip(AudioClip audioclip) {
    //    var clip = DownloadHandlerAudioClip.GetContent(request);

    //    clip.LoadAudioData();

    //    var assetName = Path.GetFileNameWithoutExtension(assetPath);
    //    var clone = AudioClip.Create("Clip", clip.samples, clip.channels, clip.frequency, false);

    //    using (var so = new SerializedObject(clone))
    //    {
    //        var samples = new float[clip.samples * clip.channels];

    //        clip.GetData(samples, 0);
    //        clone.SetData(samples, 0);

    //        // Everything above this line works fine

    //        ctx.AddObjectToAsset("AudioClip", clone);
    //        ctx.SetMainObject(clone);

    //        so.ApplyModifiedProperties();

    //        EditorUtility.SetDirty(clone);
    //    }

    IEnumerator LoadAudioSources(List<int> ayatNumbers)
    {
        audioBTN.interactable = false;
        audioDownloading.gameObject.SetActive(true);
        loading.SetActive(true);
        if (ayatNumbers != null)
            foreach (var ayaNumber in ayatNumbers)
            {
            audioDownloading.value+= 1.0f/ayatNumbers.Count+0f;
            yield return StartCoroutine(RestClient.Instance.GetVereseAudio(ayaNumber+"", result =>
            {
                Debug.Log("Aya Index: "+ayaNumber);
                if (result.status)
                {
                    foreach (var sound in result.items.sounds)
                    {
                        StartCoroutine(LoadAudio(sound.sound, ayaNumber+""));
                    }


                }
            }));
            yield return new WaitForSeconds(.3f);
        }
        audioBTN.interactable = true;
        loading.SetActive(false);
        audioDownloading.gameObject.SetActive(false);


    }
    private void OnEnable()
    {
        clips.RemoveRange(0,clips.Count);
        audioSource.clip = null;
        countIndex = 0;
        audioDownloading.value = 0;
    }
    
}
