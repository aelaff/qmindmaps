﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTextSolver : MonoBehaviour
{
    public void SolveText(string text) {
        GetComponent<ArabicText>().Text = text;
    }
}
