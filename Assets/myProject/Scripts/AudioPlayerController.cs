﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioPlayerController : MonoBehaviour
{
    public Sprite pauseIcon, playIcon;
    public Image playPauseImg;
    bool togglePuase = false;
    public Slider audioSlider;
    public AudioSource audioSource;
    public PartDetailsManager pdm;
    public int currentClip = 0;
   private void Awake()
    {
        //foreach (var at in pdm.clips)
        //{
        //    audioSlider.maxValue+= at.length;
        //}
        
        //audioSlider.minValue = 0f;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //for (int i = 0; i < pdm.clips.Count; i++)
        //{
        //    audioSlider.value = 0;
        //    if (pdm.clips[i] == audioSource.clip)
        //    {
        //        audioSlider.value+= pdm.clips[i].length + audioSource.time;
        //        break;
        //    }
            
        //}
       
        
    }
    public void PlayPauseAudio() {
        togglePuase = !togglePuase;
        if (togglePuase)
        {
            //pause
            playPauseImg.sprite = pauseIcon;
            audioSource.Pause();
        }
        else {
            //play
            playPauseImg.sprite = playIcon;
            audioSource.Play();

    
        }

    }
}
