﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SubElementController : MonoBehaviour
{
    public ArabicText subText;
    public Image shape;
    public RawImage subImage;
    public Sprite[] allShapes;
    string imgURL;
    private void Start()
    {
        StartCoroutine(DownloadImage(imgURL));
    }
    public void Setup(string subString,string imgURL,Color subjectColor)
    {
        this.imgURL = imgURL;
        if (name == "first")
        {
            shape.sprite = allShapes[0];
        }
        else if (name == "last")
        {
            shape.sprite = allShapes[1];

        }
        else {
            shape.sprite = allShapes[2];

        }
        shape.color = subjectColor;
        subText.Text = subString;
        StartCoroutine(DownloadImage(imgURL));

    }
    IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            subImage.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            //loading.SetActive(false);

        }
    }
}
