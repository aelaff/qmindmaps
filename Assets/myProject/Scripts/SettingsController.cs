﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    public PlusMinusController repeating;
    public Dropdown sheikhName;
    // Start is called before the first frame update
    private void Start()
    {
        repeating.currentVal = PlayerPrefs.GetInt("repeating", 1);
        repeating.valText.text = repeating.currentVal + "";
        if (PlayerPrefs.GetInt("sheikhID") > 0)
        {
            sheikhName.value = PlayerPrefs.GetInt("sheikhID") - 1;

        }
        else {
            sheikhName.value = 0;
        }

    }
    public void UpdateSetting() {
        PlayerPrefs.SetInt("repeating", repeating.currentVal);

    }
    public void SetSheikhID(int sheikhID) {
        PlayerPrefs.SetInt("sheikhID", sheikhID);
    }
}
