﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIEvents : MonoBehaviour
{
    public static UIEvents instance;
    public GameObject subjectGO;
    private void Start()
    {
        instance = this;
        if (Application.loadedLevel == 0)
        {
            Invoke("LoadSignIn", 5);

        }    
    }
    public void GoToScene(string sceneName) {
        RestClient.Instance.fromWhereComing = Application.loadedLevelName;
        SceneManager.LoadScene(sceneName);
    }
    public void LoadSignIn() {
        if (PlayerPrefs.GetInt(Constants.SliderShown) == 1)
        {
            SceneManager.LoadScene("SignInUp");
        }
        else
        {
            SceneManager.LoadScene(1);
            PlayerPrefs.SetInt(Constants.SliderShown, 1);
        }
        

    }
    public void OpenLink(string url) {
        Application.OpenURL(url);
    }
}
