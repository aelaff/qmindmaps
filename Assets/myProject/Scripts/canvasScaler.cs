﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class canvasScaler : MonoBehaviour
{
    public CanvasScaler Canvas;
    public List<referenceCase> cases = new List<referenceCase>();
    private void Start()
    {
        findAspectRatio();
    }
    public void findAspectRatio()
    {
        float myAspect = Camera.main.aspect;
        myAspect = float.Parse(myAspect.ToString("0.00"));
        foreach (var myCase in cases)
        {
            if (myAspect >= myCase.FromeAspectRatio && myAspect <= myCase.ToAspectRatio)
            {
                Canvas.referenceResolution = new Vector2(myCase.x, myCase.y);
            }
        }
    }
}

[Serializable]
public class referenceCase
{
    public float FromeAspectRatio, ToAspectRatio;
    public float x;
    public float y;
}
