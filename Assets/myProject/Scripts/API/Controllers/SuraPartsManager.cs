﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static Models.Sura;

public class SuraPartsManager : MonoBehaviour
{
    public GameObject loadingPanel,noDataPanel;
    public Transform parentPanel, suraElement,partsPanel,detailsPanel;
    List<Datum> Suras;
    List<Datum> AllSuras;
    public Color partsColor;
    public ArabicText welcomeName;

    public Dropdown  sheikhReading;

    private void Start()
    {

        
        loadingPanel.SetActive(true);
        Suras = new List<Datum>();
        AllSuras = new List<Datum>();
        StartCoroutine(RestClient.Instance.GetReciters(result =>
        {
            loadingPanel.SetActive(false);

            if (result.status)
            {
                sheikhReading.options.Clear();
                foreach (var item in result.items)
                {
                    sheikhReading.options.Add(new Dropdown.OptionData() { text = ArabicSupport.ArabicFixer.Fix(item.name) });
                }
            }
        }));
        Suras = RestClient.Instance.AllSuras;
        AddAllSuras();
        string username = PlayerPrefs.GetString(Constants.Full_Name);
        if(!string.IsNullOrEmpty(username))
            welcomeName.Text = "أهلا بك \n" + username;

        if (RestClient.Instance.fromWhereComing == "Map")
        {
            GetPartDetails(RestClient.Instance.partID);
            RestClient.Instance.fromWhereComing = "";
        }

    }

    public void GetSuras(string sura_name) {
        partsPanel.gameObject.SetActive(true);
        detailsPanel.gameObject.SetActive(false);
        //for (int i = 0; i < AllSuras.Count; i++)
        //{
        //    parentPanel.GetChild(i).GetComponent<SuraElement>().DeActivateAll();
        //    if (AllSuras[i].name.IndexOf(sura_name) > -1) {
        //        parentPanel.GetChild(i).GetComponent<SuraElement>().ActivateAll();
        //    }
        //}


        StartCoroutine(RestClient.Instance.GetSuras(sura_name, result =>
        {
            loadingPanel.SetActive(false);
            //Suras.RemoveRange(0, Suras.Count-1);
            RemoveAllElements();
            if (result.status)
            {
                Suras = result.items.data;
                foreach (var sura in Suras)
                {
                    SuraElement suraEl = Instantiate(suraElement, parentPanel).GetComponent<SuraElement>();
                    suraEl.gameObject.GetComponent<Button>().onClick.AddListener(() => GetSuraParts(sura.id));
                    suraEl.SuraSetup(sura.name, sura.type, sura.verse_count + "");
                }
            }
        }));
    }
    public void GetSuraParts(int suraID) {
        RemoveAllElements();

        foreach (var sura in Suras)
        {
            if (sura.id == suraID) {

                if (sura.maps.Count > 1)
                {
                    for (int i = 0; i < sura.maps.Count; i++)
                    {
                        SuraElement suraEl = Instantiate(suraElement, parentPanel).GetComponent<SuraElement>();
                        Map map = sura.maps[i];
                        suraEl.SuraSetup("الخريطة : " + (i + 1), "من " + map.form_verse + " الى" + map.to_verse, (map.to_verse- map.form_verse) + "");
                        suraEl.SetColor(partsColor);
                        suraEl.gameObject.GetComponent<Button>().onClick.AddListener(() => GetPartDetails(map.id));
                        //Debug.Log("Map ID : " + map.id);
                        
                    }
                }
                else if (sura.maps.Count == 1)
                {
                    GetPartDetails(sura.maps[0].id);
                }
                else
                {
                    noDataPanel.SetActive(true);
                    //SuraElement suraEl = Instantiate(suraElement, parentPanel).GetComponent<SuraElement>();
                    //suraEl.SuraSetup(" الجزء 1:", "", "الكل");
                    //suraEl.SetColor(partsColor);
                    //suraEl.gameObject.GetComponent<Button>().onClick.AddListener(() => GetPartDetails(-1));


                }

            }
        }

    }
    void RemoveAllElements() {
        for (int i = 0; i < parentPanel.childCount; i++)
        {
            Destroy(parentPanel.GetChild(i).gameObject);
        }
    }
    public void GetPartDetails(int partID) {
        RestClient.Instance.partID = partID;
        List<int> ayatNumbers = new List<int>();
        Debug.Log("Part ID :"+partID);
        partsPanel.gameObject.SetActive(false);
        detailsPanel.gameObject.SetActive(true);
        detailsPanel.GetComponent<PartDetailsManager>().SetDetails("","", "","","",0,null);
        loadingPanel.SetActive(true);
        if (partID == -1)
        {
            Debug.Log("Text not added yet");
        }
        else {
            StartCoroutine(RestClient.Instance.GetMaps(partID+"", result => {
                
                if (result.status) {
                    RestClient.currentMap = result;
                    string ayat = "";
                    if (result.items.childes.Count > 1)
                    {
                        foreach (var map in result.items.childes)
                        {
                            foreach (var aya in map.verses)
                            {
                                ayat += aya.text + ")" + aya.number + "(";
                                ayatNumbers.Add(aya.id);

                            }

                        }

                    }
                    else {
                        foreach (var aya in result.items.verses)
                        {
                            ayat += aya.text + ")" + aya.number + "(";
                            ayatNumbers.Add(aya.id);
                        }
                    }
                    detailsPanel.GetComponent<PartDetailsManager>().SetDetails(ayat,
                        result.items.video,
                        result.items.video2,
                        result.items.image,
                        result.items.surah.name,
                        result.items.surah.id,
                        ayatNumbers);
                    loadingPanel.SetActive(false);
                }

            }));

        }
    }
    public void Logout() {
        loadingPanel.SetActive(true);
        try
        {
            StartCoroutine(RestClient.Instance.Logout(result =>
            {
                if (result.status)
                {
                  

                    RestClient.Instance.user = null;

                }
            }));
        }
        catch (Exception ex)
        {

        }
        finally
        {
            loadingPanel.SetActive(false);
            RestClient.Instance.DeleteAllUserData();
            SceneManager.LoadScene("SignInUp");
        }
    }
    public void AddAllSuras() {
        //partsPanel.gameObject.SetActive(true);
        //detailsPanel.gameObject.SetActive(false);
        
        
        if (Suras == null)
        {
            loadingPanel.SetActive(true);
            StartCoroutine(RestClient.Instance.GetAllSuras(result =>
            {
                loadingPanel.SetActive(false);

                if (result.status)
                {
                    AllSuras = result.items.data;
                    RestClient.Instance.AllSuras = AllSuras;
                    Suras = AllSuras;
                    IntiateSuras();

                }
            }));
        }
        else {
            IntiateSuras();
        }
        

        
        

    }
    public void IntiateSuras() {
        RemoveAllElements();

        foreach (var sura in Suras)
        {
            SuraElement suraEl = Instantiate(suraElement, parentPanel).GetComponent<SuraElement>();
            suraEl.gameObject.GetComponent<Button>().onClick.AddListener(() => GetSuraParts(sura.id));
            suraEl.SuraSetup(sura.name, sura.type, sura.verse_count + "");
            suraEl.gameObject.name = sura.id + "";
        }
    }
}
