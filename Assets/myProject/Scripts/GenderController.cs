﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenderController : MonoBehaviour
{
    public Color enabledTxt,disabledTxt;
    public Image maleBTN, femaleBTN;
    public Text maleTxt, femaleTxt;
    public int genderID = 0;
    Color alphaColor = new Color(0, 0, 0, 0);
    Color enabledBTNColor=Color.white;

    public void SetEnabledBTN(int gender) {
        genderID = gender;
        if (gender == 0) {
            maleBTN.color = enabledBTNColor;
            maleTxt.color = enabledTxt;
            femaleBTN.color = alphaColor;
            femaleTxt.color = disabledTxt;
        } else if (gender == 1) {
            maleBTN.color = alphaColor;
            maleTxt.color = disabledTxt;
            femaleBTN.color = enabledBTNColor;
            femaleTxt.color = enabledTxt;
        }
    }

}
